# Calculadora d’enllaços ràdio

La major part de la xarxa Guifi.net està formada per dispositius de xarxa (routers) enllaçats amb tecnologia ràdio de banda comuna de 2.4 GHz o 5 GHz. Degut a la saturació de 2.4 GHz la majoria d'enllaços es fan a 5 GHz. Aquesta calculadora dóna importància a dimensionar correctament radio enllaços de la banda de 5 GHz en funció del maquinari que es vulgui emprar i de la distància entre els punts que cal enllaçar.
